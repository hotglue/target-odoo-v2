"""OdooV2 target class."""

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_odoo_v2.sinks import OdooV2Sink


class TargetOdooV2(Target):
    """Sample target for OdooV2."""

    name = "target-odoo-v2"
    config_jsonschema = th.PropertiesList(
        th.Property("db", th.StringType, required=True),
        th.Property("url", th.StringType, required=True),
        th.Property("username", th.StringType, required=True),
        th.Property("password", th.StringType, required=True),
    ).to_dict()
    default_sink_class = OdooV2Sink


if __name__ == "__main__":
    TargetOdooV2.cli()
